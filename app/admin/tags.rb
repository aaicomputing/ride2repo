ActiveAdmin.register Tag do
  permit_params :name, :picker

  index do
    column :name
    column :picker
    default_actions
  end

  form do |f|
    f.inputs "Tag details" do
      f.input :name
      f.input :picker
    end
    f.actions
  end

end
