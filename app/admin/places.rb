ActiveAdmin.register Place do
  permit_params :title, :description, :latitude, :longtitude, :objdiff, :ridediff, :photo

  index do
    column :title
    column :description
    column :latitude
    column :longtitude
    column :objdiff
    column :ridediff
    default_actions
  end
  filter :title
  filter :objdiff
  filter :ridediff
  form do |f|
    f.inputs "Place details" do
      f.input :title
      f.input :description
      f.input :latitude
      f.input :longtitude
      f.input :objdiff
      f.input :ridediff
      f.input :photo, :hint => f.template.image_tag(f.object.photo.url(:thumb))
    end
    f.actions
  end

end
