class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :title
      t.string :description
      t.decimal :latitude, :precision => 8, :scale => 6
      t.decimal :longtitude, :precision => 8, :scale => 6
      t.integer :objdiff
      t.integer :ridediff
      t.timestamps
    end
  end
end

