class Placeaddphoto < ActiveRecord::Migration
  def change
    add_attachment :places, :photo
  end
end